package pl.sdacademy.session;

import java.util.Optional;

public class CustomerSession {
    private static CustomerSession session;
    private Optional<String> loggedCustomer = Optional.empty();

    private CustomerSession() {}

    public static CustomerSession getSession() {
        if (session == null) {
            session = new CustomerSession();
        }

        return session;
    }

    public void login(String customerLogin) {
        loggedCustomer = Optional.ofNullable(customerLogin);
    }

    public void logout() {
        loggedCustomer = Optional.empty();
    }

    public Optional<String> getLoggedCustomer() {
        return loggedCustomer;
    }
}
