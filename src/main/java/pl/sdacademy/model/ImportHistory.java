package pl.sdacademy.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ImportHistory {
    private LocalDateTime importedDate;
    private List<Customer> savedCustomers = new ArrayList<>();
    private List<Customer> updatedCustomers = new ArrayList<>();
    private int omittedCustomersCount = 0;

    public LocalDateTime getImportedDate() {
        return importedDate;
    }

    public void setImportedDate(LocalDateTime importedDate) {
        this.importedDate = importedDate;
    }

    public List<Customer> getSavedCustomers() {
        return savedCustomers;
    }

    public void setSavedCustomers(List<Customer> savedCustomers) {
        this.savedCustomers = savedCustomers;
    }

    public List<Customer> getUpdatedCustomers() {
        return updatedCustomers;
    }

    public void setUpdatedCustomers(List<Customer> updatedCustomers) {
        this.updatedCustomers = updatedCustomers;
    }

    public int getOmittedCustomersCount() {
        return omittedCustomersCount;
    }

    public void setOmittedCustomersCount(int omittedCustomersCount) {
        this.omittedCustomersCount = omittedCustomersCount;
    }

    public int getUpdatedCustomersCount() {
        return updatedCustomers.size();
    }

    public int getSavedCustomersCount() {
        return savedCustomers.size();
    }
}
