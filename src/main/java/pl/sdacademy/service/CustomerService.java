package pl.sdacademy.service;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import pl.sdacademy.model.Customer;
import pl.sdacademy.model.CustomerSaveStatus;
import pl.sdacademy.parser.CustomerParser;
import pl.sdacademy.utils.CustomerValidation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.*;

public class CustomerService {

    private MongoCollection<Document> collection;

    public CustomerService(MongoClient mongoClient) {
        collection = mongoClient.getDatabase("customersDatabase").getCollection("customer");
    }

    public CustomerSaveStatus save(Customer customer) {
        Optional<Customer> customerInDB = getByLogin(customer.getLogin());

        if (customerInDB.isPresent()) {
            customer.setDateCreated(customerInDB.get().getDateCreated());
            customer.setDateModified(LocalDateTime.now());
            collection.updateOne(
                    eq("login",customer.getLogin()),
                    new Document("$set",CustomerParser.getDocument(customer)),
                    new UpdateOptions().upsert(true));
            return CustomerSaveStatus.UPDATED;
        } else if (CustomerValidation.isValid(customer)){
            customer.setDateCreated(LocalDateTime.now());
            customer.setDateModified(LocalDateTime.now());
            customer.setActive(true);
            collection.insertOne(CustomerParser.getDocument(customer));
            return CustomerSaveStatus.SAVED;
        }
        return CustomerSaveStatus.OMMITED;
    }

    public Optional<Customer> getByLogin(String login) {
        FindIterable<Document> c = collection.find(eq("login",login));

        if (c.iterator().hasNext())
            return Optional.of(CustomerParser.getCustomer(c.iterator().next()));
        else
            return Optional.empty();
    }

    public void setCustomerInActive(String login) {
        getByLogin(login).ifPresent(customer -> {
            customer.setActive(false);
            save(customer);
        });
    }

    public Optional<Customer> getByLoginAndPassword(String login, String password) {
        FindIterable<Document> c = collection.find(new Document()
                                .append("login",login)
                                .append("password",password));
        if (c.iterator().hasNext())
            return Optional.of(CustomerParser.getCustomer(c.iterator().next()));
        else
            return Optional.empty();
    }

    public List<Customer> getAll() {
        MongoCursor<Document> cursor = collection.find().iterator();
        List<Customer> customers = new ArrayList<>();

        while(cursor.hasNext()) {
            customers.add(CustomerParser.getCustomer(cursor.next()));
        }

        return customers;
    }
}
