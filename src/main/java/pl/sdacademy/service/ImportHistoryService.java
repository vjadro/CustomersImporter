package pl.sdacademy.service;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import pl.sdacademy.model.ImportHistory;
import pl.sdacademy.parser.ImportHistoryParser;

import java.util.ArrayList;
import java.util.List;

public class ImportHistoryService {

    private MongoCollection<Document> collection;

    public ImportHistoryService(MongoClient mongoClient) {
        collection = mongoClient.getDatabase("customersDatabase").getCollection("importHistory");
    }

    public void save(ImportHistory importHistory) {
        collection.insertOne(ImportHistoryParser.getDocument(importHistory));
    }

    public List<ImportHistory> getAll() {

        List<ImportHistory> importHistoryList = new ArrayList<>();
        for (Document document : collection.find()) {
            importHistoryList.add(ImportHistoryParser.getImportHistory(document));
        }

        return importHistoryList;
    }
}
