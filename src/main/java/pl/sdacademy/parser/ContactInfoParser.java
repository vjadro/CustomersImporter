package pl.sdacademy.parser;

import org.bson.Document;
import pl.sdacademy.model.ContactInfo;

public class ContactInfoParser {

    public static Document getDocument(ContactInfo contactInfo) {
        return new Document()
                .append("phone",contactInfo.getPhone())
                .append("fax",contactInfo.getFax())
                .append("email",contactInfo.getEmail())
                .append("web",contactInfo.getWeb());
    }

    public static ContactInfo getContactInfo(Document document) {
        ContactInfo contactInfo = new ContactInfo();
        contactInfo.setPhone(document.getString("phone"));
        contactInfo.setFax(document.getString("fax"));
        contactInfo.setEmail(document.getString("email"));
        contactInfo.setWeb(document.getString("web"));
        return contactInfo;
    }
}
