package pl.sdacademy.parser;

import org.bson.Document;
import pl.sdacademy.model.Address;

public class AddressParser {

    public static Document getDocument(Address address) {
        return new Document()
                .append("street",address.getStreet())
                .append("city",address.getCity())
                .append("county",address.getCounty())
                .append("state",address.getState())
                .append("zip",address.getZip());
    }

    public static Address getAddress(Document document) {
        Address address = new Address();
        address.setStreet(document.getString("street"));
        address.setCity(document.getString("city"));
        address.setCounty(document.getString("county"));
        address.setState(document.getString("state"));
        address.setZip(document.getString("zip"));
        return address;
    }
}
