package pl.sdacademy.parser;

import org.bson.Document;
import pl.sdacademy.model.Customer;
import pl.sdacademy.model.ImportHistory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ImportHistoryParser {

    public static ImportHistory getImportHistory(Document document) {
        ImportHistory importHistory = new ImportHistory();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        importHistory.setImportedDate(LocalDateTime.parse(document.getString("importedDate"),formatter));
        importHistory.setOmittedCustomersCount(document.getInteger("ommitedCustomersCount"));
        importHistory.setSavedCustomers((List<Customer>) document.get("savedCustomers"));
        importHistory.setUpdatedCustomers((List<Customer>) document.get("updatedCustomers"));
        return importHistory;
    }

    public static Document getDocument(ImportHistory importHistory) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return new Document()
                .append("importedDate",importHistory.getImportedDate().format(formatter))
                .append("ommitedCustomersCount",importHistory.getOmittedCustomersCount())
                .append("savedCustomers",customerListToDocument(importHistory.getSavedCustomers()))
                .append("updatedCustomers",customerListToDocument(importHistory.getUpdatedCustomers()));
    }

    private static List<Document> customerListToDocument(List<Customer> customers) {
        List<Document> documentList = new ArrayList<>();

        for (Customer customer : customers) {
            documentList.add(CustomerParser.getDocument(customer));
        }

        return documentList;
    }
}
