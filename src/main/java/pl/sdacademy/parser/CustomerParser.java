package pl.sdacademy.parser;

import org.bson.Document;
import pl.sdacademy.model.Customer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CustomerParser {

    public static Document getDocument(Customer customer) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatCreated = customer.getDateCreated().format(formatter);
        String formatModified = customer.getDateModified().format(formatter);

        return new Document()
                .append("login",customer.getLogin())
                .append("password",customer.getPassword())
                .append("firstName",customer.getFirstName())
                .append("lastName",customer.getLastName())
                .append("company",customer.getCompany())
                .append("address",AddressParser.getDocument(customer.getAddress()))
                .append("contactInfo",ContactInfoParser.getDocument(customer.getContactInfo()))
                .append("dateCreated",formatCreated)
                .append("dateModified", formatModified)
                .append("active",customer.isActive());
    }

    public static Customer getCustomer(Document document) {
        Customer customer = new Customer();
        customer.setLogin(document.getString("login"));
        customer.setPassword(document.getString("password"));
        customer.setFirstName(document.getString("firstName"));
        customer.setLastName(document.getString("lastName"));
        customer.setCompany(document.getString("company"));
        customer.setAddress(AddressParser.getAddress((Document) document.get("address")));
        customer.setContactInfo(ContactInfoParser.getContactInfo((Document) document.get("contactInfo")));
        customer.setActive(document.getBoolean("active"));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        customer.setDateCreated(LocalDateTime.parse(document.getString("dateCreated"),formatter));
        customer.setDateModified(LocalDateTime.parse(document.getString("dateModified"),formatter));

        return customer;
    }
}
