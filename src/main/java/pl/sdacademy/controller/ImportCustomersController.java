package pl.sdacademy.controller;

import pl.sdacademy.file.CustomersFileReader;
import pl.sdacademy.model.Customer;
import pl.sdacademy.model.CustomerSaveStatus;
import pl.sdacademy.model.ImportHistory;
import pl.sdacademy.service.CustomerService;
import pl.sdacademy.service.ImportHistoryService;

import java.io.IOException;
import java.time.LocalDateTime;

public class ImportCustomersController {

    private ImportHistoryService importHistoryService;
    private CustomerService customerService;

    public ImportCustomersController(ImportHistoryService importHistoryService, CustomerService customerService) {
        this.importHistoryService = importHistoryService;
        this.customerService = customerService;
    }

    public void importCustomersFromFile() throws IOException {
        ImportHistory importHistory = new ImportHistory();
        importHistory.setImportedDate(LocalDateTime.now());

        int ommited = 0;

        for(Customer customer : CustomersFileReader.getCustomersFromCsv()) {
            CustomerSaveStatus status = customerService.save(customer);
            switch(status) {
                case OMMITED:
                    ommited++;
                    break;
                case SAVED:
                    importHistory.getSavedCustomers().add(customer);
                    break;
                case UPDATED:
                    importHistory.getUpdatedCustomers().add(customer);
                    break;
                default:
            }
        }

        importHistory.setOmittedCustomersCount(ommited);
        importHistoryService.save(importHistory);
    }
}
