package pl.sdacademy.controller;

import pl.sdacademy.model.Customer;
import pl.sdacademy.service.CustomerService;
import pl.sdacademy.session.CustomerSession;

import java.util.List;
import java.util.Optional;

public class CustomerController {

    private CustomerService service;

    public CustomerController(CustomerService service) {
        this.service = service;
    }

    public void save(Customer customer) {
        service.save(customer);
    }

    public List<Customer> getAll() {
        return service.getAll();
    }

    public void login(String login, String password) {
        Optional<Customer> customer = service.getByLoginAndPassword(login,password);
        customer.ifPresent(customer1 -> {
            if (customer1.isActive())
                CustomerSession.getSession().login(customer1.getLogin());
        });
    }

    public Optional<Customer> getLoggedCustomer() {
        Optional<String> customerLogin = CustomerSession.getSession().getLoggedCustomer();
        if (customerLogin.isPresent())
            return service.getByLogin(customerLogin.get());
        else
            return Optional.empty();
    }

    public void setCustomerInactive(String login) {
        service.setCustomerInActive(login);
    }
}
