package pl.sdacademy;

import com.mongodb.MongoClient;
import pl.sdacademy.controller.CustomerController;
import pl.sdacademy.controller.ImportCustomersController;
import pl.sdacademy.file.CustomersFileReader;
import pl.sdacademy.model.Customer;
import pl.sdacademy.service.CustomerService;
import pl.sdacademy.service.ImportHistoryService;
import pl.sdacademy.session.CustomerSession;

import java.io.IOException;

public class App
{
    public static void main( String[] args ) throws IOException {

        MongoClient mongoClient = new MongoClient();

        CustomerService customerService = new CustomerService(mongoClient);
        ImportHistoryService importHistoryService = new ImportHistoryService(mongoClient);

        CustomerController customerController = new CustomerController(customerService);
        ImportCustomersController importCustomersController =
                new ImportCustomersController(importHistoryService,customerService);

        importCustomersController.importCustomersFromFile();

        //customerController.setCustomerInactive("willard@roughen.com");

        customerController.login("willard@roughen.com","wroughen");

        customerController.getLoggedCustomer().ifPresent(customer -> System.out.println(customer.getLogin()));
    }
}
