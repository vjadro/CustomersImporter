package pl.sdacademy.utils;

import org.apache.commons.lang3.StringUtils;
import pl.sdacademy.model.Customer;

public class CustomerValidation {

    public static boolean isValid(Customer customer) {
        if (customer.getContactInfo() == null)
            return false;

        return !StringUtils.isEmpty(customer.getContactInfo().getEmail())
                && !StringUtils.isEmpty(customer.getFirstName())
                && !StringUtils.isEmpty(customer.getLastName());
    }
}
