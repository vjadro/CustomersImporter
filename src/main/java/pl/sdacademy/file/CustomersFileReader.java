package pl.sdacademy.file;

import com.opencsv.CSVReader;
import pl.sdacademy.model.Address;
import pl.sdacademy.model.ContactInfo;
import pl.sdacademy.model.Customer;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CustomersFileReader {

    private static final String CUSTOMERS_CSV_FILE_PATH = "resources/customers.csv";
    private static final int CUSTOMER_FIRST_NAME = 0;
    private static final int CUSTOMER_LAST_NAME = 1;
    private static final int CUSTOMER_COMPANY = 2;
    private static final int CUSTOMER_STREET = 3;
    private static final int CUSTOMER_CITY = 4;
    private static final int CUSTOMER_COUNTY = 5;
    private static final int CUSTOMER_STATE = 6;
    private static final int CUSTOMER_ZIP = 7;
    private static final int CUSTOMER_PHONE = 8;
    private static final int CUSTOMER_FAX = 9;
    private static final int CUSTOMER_EMAIL = 10;
    private static final int CUSTOMER_WEB = 11;

    public static List<Customer> getCustomersFromCsv() throws IOException {
        List<Customer> customers = new ArrayList<>();

        try (
                Reader reader = Files.newBufferedReader(Paths.get(CUSTOMERS_CSV_FILE_PATH));
                CSVReader csvReader = new CSVReader(reader);
        ) {
            // Reading Records One by One in a String array
            String[] nextRecord;

            csvReader.readNext();

            while ((nextRecord = csvReader.readNext()) != null) {
                customers.add(parseCustomer(nextRecord));
            }
        }

        return customers;
    }

    private static Customer parseCustomer(String[] row) {
        Customer customer = new Customer();
        customer.setFirstName(row[CUSTOMER_FIRST_NAME]);
        customer.setLastName(row[CUSTOMER_LAST_NAME]);
        customer.setCompany(row[CUSTOMER_COMPANY]);

        Address address = new Address();
        address.setStreet(row[CUSTOMER_STREET]);
        address.setCity(row[CUSTOMER_CITY]);
        address.setCounty(row[CUSTOMER_COUNTY]);
        address.setCounty(row[CUSTOMER_STATE]);
        address.setCounty(row[CUSTOMER_ZIP]);

        ContactInfo contactInfo = new ContactInfo();
        contactInfo.setPhone(row[CUSTOMER_PHONE]);
        contactInfo.setFax(row[CUSTOMER_FAX]);
        contactInfo.setEmail(row[CUSTOMER_EMAIL]);
        contactInfo.setWeb(row[CUSTOMER_WEB]);

        customer.setAddress(address);
        customer.setContactInfo(contactInfo);

        customer.setLogin(row[CUSTOMER_EMAIL]);
        customer.setPassword(generatePassword(row[CUSTOMER_FIRST_NAME],row[CUSTOMER_LAST_NAME]));

        return customer;
    }

    private static String generatePassword(String fName, String lName) {
        return (String
                .valueOf(fName.charAt(0)) + lName)
                .toLowerCase();
    }
}
